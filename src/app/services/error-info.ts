export interface ErrorInfo {
    errorCode: number
    errorMsg: String
}
