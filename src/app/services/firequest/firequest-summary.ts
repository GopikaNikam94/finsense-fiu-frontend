export interface FiRequestSummary {
  custId: String
  consentId: String
  consentHandleId: String
  dateTimeRangeFrom: Date
  dateTimeRangeTo: Date
}

export interface FiResponseSummary {
  consentId: String
  sessionId: String
}

export interface UserRequest {
  custId: String
  consentHandleId: String
  status: String
}