import { TestBed, inject } from '@angular/core/testing';

import { ConsentTemplateService } from './consent-template.service';

describe('ConsentRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsentTemplateService]
    });
  });

  it('should be created', inject([ConsentTemplateService], (service: ConsentTemplateService) => {
    expect(service).toBeTruthy();
  }));
});
