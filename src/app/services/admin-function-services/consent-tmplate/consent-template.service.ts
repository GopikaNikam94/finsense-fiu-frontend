import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from 'src/app/services/web-response';
import { WebHeader } from 'src/app/services/web-header';
import { WebRequest } from 'src/app/services/web-request';
import { ConfigService } from 'src/app/services/config/config-service';
import { Observable, throwError } from 'rxjs';
import { ConsentTemplateRequest, ApproveConsentTemplate } from './consent-template';
import { retry, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ConsentTemplateService {

  apiBaseUrl: string;

  constructor(private http: HttpClient,
              private configService: ConfigService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
    }

  createConsentTemplate(consentTemplateRequest: ConsentTemplateRequest): Observable<ConsentTemplateRequest>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: consentTemplateRequest
    };
    return this.http.post<ConsentTemplateRequest>(this.apiBaseUrl +'/ConsentTemplate',request).pipe(retry(1),
    catchError(this.handleError));
  }

  approveConsentTemplate(approveConsentTemplate: ApproveConsentTemplate): Observable<ConsentTemplateRequest>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: approveConsentTemplate
    };
    return this.http.post<ConsentTemplateRequest>(this.apiBaseUrl +'/ConsentTemplate/Approved',request).pipe(retry(1),
    catchError(this.handleError));
  }

  updateApprovedConsentTemplate(consentTemplateRequest: ConsentTemplateRequest): Observable<ConsentTemplateRequest>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: consentTemplateRequest
    };
    return this.http.put<ConsentTemplateRequest>(this.apiBaseUrl +'/ConsentTemplate/Approved',request).pipe(retry(1),
    catchError(this.handleError));
  }
  
  fetchConsentTemplateList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/ConsentTemplates').pipe(retry(1),
      catchError(this.handleError));
  }

  fetchPurpose(purposeCode) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/purpose/' +purposeCode).pipe(retry(1),
      catchError(this.handleError));
  }
      
  fetchConsentTemplate(templateName) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/ConsentTemplate/'+ templateName).pipe(retry(1),
      catchError(this.handleError));
  }
  
  fetchApprovedConsentTemplate(templateName) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/ConsentTemplate/Approved/' +templateName).pipe(retry(1),
      catchError(this.handleError));
  }

  fetchApprovedConsentTemplateList() {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/ConsentTemplates/Approved').pipe(retry(1),
      catchError(this.handleError));
  }
  
  deleteConsentTemplate(templateName) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/ConsentTemplate/' +templateName).pipe(retry(1),
      catchError(this.handleError));
  }

  deleteApprovedConsentTemplate(templateName) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/ConsentTemplate/Approved/' +templateName).pipe(retry(1),
      catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      window.alert("JWT expired Please login again!");
      window.location.href = '/login'
    } else if(error.status == 500) {
      window.alert("Something went wrong!");
    }
    return throwError(errorMessage);
   }
}
