export interface ConsentTemplateRequest {
  templateName:         String
  templateDescription:  String
  consentMode:          String
  consentTypes:         any
  fiTypes:              any
  Purpose:              Purpose
  fetchType:            String
  Frequency:            Frequency
  DataLife:             DataLife
  ConsentExpiry:        ConsentExpiry
  consentStartDays:     number
  dataRangeStartMonths: number
  dataRangeEndMonths:   number
  createdBy:            string
  creationDate:         Date
  updatedBy:            string
  updationDate:         Date
  approvedBy:           string
  approvedDate:         Date
}

export interface ApproveConsentTemplate {
  templateName:         String
}

export interface Purpose {
  code: String
  refUri: String
  text: String
  Category: Category
}

export interface Category {
  type: String
}

export interface Frequency {
  unit: String
  value: number
}

export interface DataLife {
  unit: String
  value: number
}

export interface ConsentExpiry {
  unit: String
  value: number
}

export class FiType {
  fiType:String 
  fiTypeDescription:String 
}
	
export interface IDataOption {
  value : string;
  disabled? : boolean;
}
