import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from 'src/app/services/web-response';
import { WebHeader } from 'src/app/services/web-header';
import { WebRequest } from 'src/app/services/web-request';
import { Channel, ApproveChannel } from './channel';
import { ConfigService } from '../../config/config-service';
import { Observable , throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {
  
  public apiBaseUrl: string;

  constructor(private http: HttpClient,
              private configService: ConfigService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
    }

  createChannel(channel: Channel): Observable<Channel>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: channel
    };
    return this.http.post<Channel>(this.apiBaseUrl +'/Channel',request).pipe(retry(1),
    catchError(this.handleError));
  }

  createApprovedChannel(approveChannel: ApproveChannel): Observable<ApproveChannel>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: approveChannel
    };
    return this.http.post<ApproveChannel>(this.apiBaseUrl +'/Channel/Approved',request).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchChannelList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Channels').pipe(retry(1),
      catchError(this.handleError));
  }
  
  fetchApprovedChannelList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Channels/Approved').pipe(retry(1),
      catchError(this.handleError));
  }

  updateChannel(channel: Channel): Observable<Channel>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: channel
    };
    return this.http.put<Channel>(this.apiBaseUrl +'/Channel',request).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchChannel(channelId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Channel/' + channelId).pipe(retry(1),
      catchError(this.handleError));
  }

  fetchApprovedChannel(channelId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Channel/Approved/' + channelId).pipe(retry(1),
      catchError(this.handleError));
  }

  deleteChannel(channelId){
    return this.http.delete<WebResponse>(this.apiBaseUrl + '/Channel/' + channelId).pipe(retry(1),
    catchError(this.handleError));
  }

  deleteApprovedChannel(channelId){
    return this.http.delete<WebResponse>(this.apiBaseUrl + '/Channel/Approved/' + channelId).pipe(retry(1),
    catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      window.alert("JWT expired Please login again!");
      window.location.href = '/login'
    } else if(error.status == 500) {
      window.alert("Something went wrong!");
    }
    return throwError(errorMessage);
   }
}
