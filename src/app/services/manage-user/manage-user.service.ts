import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import { WebHeader } from '../web-header';
import { WebRequest } from '../web-request';
import { ConfigService } from '../config/config-service';
import { Observable , throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SimpleModalService } from 'ngx-simple-modal';
import { ManageUser, ApproveManageUser } from './manage-user'
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class ManageUserService {

  apiBaseUrl: string;

  constructor(public http: HttpClient, 
    public configService: ConfigService,
    public router: Router,
    public SimpleModalService: SimpleModalService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
  }

  createUser(manageUser : ManageUser): Observable<ManageUser>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: manageUser
    };
    return this.http.post<ManageUser>(this.apiBaseUrl +'/Users', request).pipe(retry(1),
    catchError(this.handleError));
  }

  approveUser(approveManageUser : ApproveManageUser): Observable<ApproveManageUser>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: approveManageUser
    };
    return this.http.post<ApproveManageUser>(this.apiBaseUrl +'/Users/Approved', request).pipe(retry(1),
    catchError(this.handleError));
  }

  updateApprovedUserDetails(manageUser : ManageUser): Observable<ManageUser>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: manageUser
    };
    return this.http.put<ManageUser>(this.apiBaseUrl +'/Users/Approved', request).pipe(retry(1),
    catchError(this.handleError));
  }
  
  fetchRoleList(){
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Users/Role').pipe(retry(1),
    catchError(this.handleError));
  }
  
  fetchUserDetails(userId) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Users/' + userId).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchApprovedUserDetails(userId) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Users/Approved/' +userId).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchUserList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Users').pipe(retry(1),
    catchError(this.handleError));
  }

  fetchApprovedUserList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/Users/Approved').pipe(retry(1),
    catchError(this.handleError));
  }

  deleteUser(userId) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/Users/' +userId).pipe(retry(1),
    catchError(this.handleError));
  }
  
  deleteApprovedUser(userId) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/Users/Approved/' +userId).pipe(retry(1),
    catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      console.log("JWT expired!");
      window.location.href = '/login'
    } 
    return throwError(errorMessage);
  }
}
