
export interface FiDetail {
    name: String
  }

export interface Row {
    category: string;
    amount: number;
}

export interface TotalDebitCredit {
    Rows: Row[];
    totalAmount: number;
}

export interface FlowAnalysis {
    totalCredits: TotalDebitCredit;
    totalDebits: TotalDebitCredit;
    netFlow: number;
}
