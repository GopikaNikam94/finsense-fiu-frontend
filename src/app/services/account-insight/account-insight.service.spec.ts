import { TestBed, inject } from '@angular/core/testing';

import { AccountInsightService } from './account-insight.service';

describe('AccountInsightService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountInsightService]
    });
  });

  it('should be created', inject([AccountInsightService], (service: AccountInsightService) => {
    expect(service).toBeTruthy();
  }));
});
