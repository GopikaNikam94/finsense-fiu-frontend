import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PendingConsentTemplateComponent, NgbdModalApprove, NgbdModalReject } from './pending-consent-template.component';
import { PendingConsentTemplateRoutingModule } from './pending-consent-template-routing.module';
import { SelectModule } from 'ng-select';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    PendingConsentTemplateRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ PendingConsentTemplateComponent, NgbdModalApprove, NgbdModalReject ],
  providers: [ SelectOptionService ], 
  bootstrap: [ PendingConsentTemplateComponent ],
  entryComponents: [ NgbdModalApprove, NgbdModalReject ]
})
export class PendingConsentTemplateModule { }



