import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PendingConsentTemplateComponent } from './pending-consent-template.component';

describe('ConsentTemplatesComponent', () => {
  let component: PendingConsentTemplateComponent;
  let fixture: ComponentFixture<PendingConsentTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingConsentTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingConsentTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
