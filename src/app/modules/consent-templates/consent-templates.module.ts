import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsentTemplatesRoutingModule } from './consent-templates-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ConsentTemplatesRoutingModule
  ],
  declarations: []
})
export class ConsentTemplatesModule { }
