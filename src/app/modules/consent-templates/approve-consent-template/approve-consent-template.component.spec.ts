import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveConsentTemplateComponent } from './approve-consent-template.component';

describe('ApproveConsentTemplateComponent', () => {
  let component: ApproveConsentTemplateComponent;
  let fixture: ComponentFixture<ApproveConsentTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveConsentTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveConsentTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
