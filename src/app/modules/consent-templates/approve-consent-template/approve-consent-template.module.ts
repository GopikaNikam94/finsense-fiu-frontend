import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApproveConsentTemplateComponent, NgbdModalDelete } from './approve-consent-template.component';
import { ApproveConsentTemplateRoutingModule } from './approve-consent-template-routing.module';
import { SelectModule } from 'ng-select';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ApproveConsentTemplateRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ ApproveConsentTemplateComponent, NgbdModalDelete ],
  providers: [ SelectOptionService ],
  bootstrap: [ ApproveConsentTemplateComponent ],
  entryComponents: [ NgbdModalDelete ]
})
export class ApproveConsentTemplateModule { }



