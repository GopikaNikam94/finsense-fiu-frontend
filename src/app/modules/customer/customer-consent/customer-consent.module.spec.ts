import { CustomerConsentModule } from './customer-consent.module';

describe('CustomerConsentModule', () => {
  let customerConsentModule: CustomerConsentModule;

  beforeEach(() => {
    customerConsentModule = new CustomerConsentModule();
  });

  it('should create an instance', () => {
    expect(customerConsentModule).toBeTruthy();
  });
});
