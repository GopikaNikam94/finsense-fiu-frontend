import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ConsentRequestService } from '../../../services/consent-request/consent-request.service';
import { FiRequestService } from '../../../services/firequest/firequest.service';
import { FiRequestSummary } from '../../../services/firequest/firequest-summary';
import { animate, style, transition, trigger } from '@angular/animations';
import { AccountInsightService } from '../../../services/account-insight/account-insight.service';
import { FiDetail, FlowAnalysis } from '../../../services/account-insight/account-insight-summary';
import { Subscription } from 'rxjs';
import { SimpleModalService } from 'ngx-simple-modal';
import { Router } from '@angular/router';
import { AlertComponent } from '../../alert/alert.component';
import { ConsentStatus } from 'src/app/services/helpers/consent-status';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})

export class CustomerListComponent implements OnInit {
  public subscription: Subscription; customerConsentForm: FormGroup;
  public fiDetail: FiDetail; flowAnalysis: FlowAnalysis;
  public consentHandleId: String; custId: String; consentStatus: String;
  public fiObjectsData: any; fiObjects: any; fipId: any; timeout: any; creditPercent: any;
  debitPercent: any; flowAnaylsisPieChartData: any; result: any; modal: any;
  fiRequestSummaryData: any; resultKey: any; resultData: any
  public consentDetailsPage = true; transactionDetailsPage = true; collapsed = true; collapsed1 = true;
  isRequest = true; collapsed2 = true; accountsInsightPage = true; checkData = true; flagStatus = true
  public depositType: boolean = false; termDeposiType: boolean = false; depositIfsc: boolean = false;
  termDepositIfsc: boolean = false; depositTrasactionTimestamp: boolean = false;
  termDepositTransactionDateTime: boolean = false; depositCurrentBalance: boolean = false;
  termDepositCurrentValue: boolean = false; isCustomerResult = false; isConsentDateResult = false;
  isConsentDateAndCustomerResult = false;
  public consentDetailsList = []; tempConsentDetailsList = []; transactions = []; overdrafts = []
  autoDebits = []; monthlySurplus = []; monthlyAvgBalance = []; recurringCredits = []; topCredits = [];
  topDebits = []; recurringDebits = []; topCashTxns = []; topChequeTxns = []; data1 = [];
  public maxSize = 5; bigTotalItems = 175; bigTotalItemsLarge = 30;
  public period = {};
  public custAAId; consentRequestDate;

  public TAKE_VALUE_FROM_JSON_NAME = {
    CONSENT_DETAILS_LIST: "consentDetails"
  };

  ngOnInit() { }

  savingsPieChartData = {
    chartType: 'PieChart',
    dataTable: [
      ['Task', 'Percentage'],
      ['Bank of B public fiRequestService: FiRequestServicearoda', 35],
      ['IndusInd Bank', 40],
      ['State Bank of India', 25]
    ],
    options: {
      height: 300,
      pieHole: 0.3,
      colors: ['rgb(236, 173, 56)', '#E01563', '#44bff0'],
      legend: { position: 'right', maxLines: 2 }
    },
  };

  savingsLineChartData = {
    chartType: 'AreaChart',
    dataTable: [
      ['Value', 'Bank of Baroda', 'IndusInd Bank', 'State Bank of India'],
      ['Investment  Value', 1113, 1120, 800],
      ['Market  Value', 450, 950, 800],
      ['Gain  Value', 400, 749, 700],
      ['Total  Investment', 1513, 1869, 1500]
    ],
    options: {
      vAxis: { minValue: 0 },
      colors: ['rgb(236, 173, 56)', '#E01563', '#44bff0'],
      height: 300,
      isStacked: true,
      legend: { position: 'top', maxLines: 2 }
    },
  };

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private consentRequestService: ConsentRequestService,
    private fiRequestService: FiRequestService,
    private simpleModalService: SimpleModalService,
    private accountInsightService: AccountInsightService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.customerConsentForm = this.formBuilder.group({
      customerId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      consentRequestDate: ['', Validators.required]
    })
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  onCheckStatus(row) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.checkConsentStatus(row.consentHandle, row.custId).subscribe(data => {
        console.log("Check consent status response" + JSON.stringify(data));
        this.getConsentStaus(row.consentHandle);
        var response = data['body']
        if (response['consentStatus'] == ConsentStatus.PENDING) {
          row.consentStatus = ConsentStatus.REQUESTED;
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please approve consent',
          })
        } else {
          row.consentStatus = response['consentStatus'];
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  onFiRequest(row) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.getConsentDetailsById(row.consentHandle).subscribe(data => {
        var response = data['body'];
        if (response['consentStatus'] == ConsentStatus.REQUESTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please approve consent',
          })
        } else if (response['consentStatus'] == ConsentStatus.REVOKED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is revoked, You can not send data request',
          })
        } else if (response['consentStatus'] == ConsentStatus.REJECTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is rejected',
          })
        } else {
          console.log("Consent Status before data request: " + data['consentStatus']);

          const fiRequestSummary: FiRequestSummary = {
            custId: row.custId,
            consentId: response['requestConsentId'],
            consentHandleId: row.consentHandle,
            dateTimeRangeFrom: response['dateTimeRangeFrom'],
            dateTimeRangeTo: response['dateTimeRangeTo']
          }
          console.log("FiRequestSummary: ", JSON.stringify(fiRequestSummary));
          sessionStorage.setItem("data", JSON.stringify(fiRequestSummary))
          this.fiRequestSummaryData = sessionStorage.getItem("data");

          if (response['consentStatus'] == ConsentStatus.DATA_REQUESTED) {
            row.consentStatus = response['consentStatus']
            // this.simpleModalService.addModal(AlertComponent, {
            //   message: 'Do you want send data request?'
            // })
            //this.callFiRequest(row);
            if (confirm("Do you want send data request?")) {
              this.callFiRequest(row);
            } else {

            }
          } else if (response['consentStatus'] == ConsentStatus.DATA_READY) {
            row.consentStatus = ConsentStatus.DATA_REQUESTED;
            // this.simpleModalService.addModal(AlertComponent, {
            //   message: 'Do you want send data request?'
            // })
            if (confirm("Do you want send data request?")) {
              this.callFiRequest(row);
            } else {

            }

          } else if (response['consentStatus'] == ConsentStatus.REVOKED) {
            row.consentStatus = ConsentStatus.REVOKED;
          } else if (response['consentStatus'] == ConsentStatus.EXPIRED) {
            row.consentStatus = ConsentStatus.EXPIRED;
          } else {
            this.callFiRequest(row);
          }
        }
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  getFIstatus(row) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.getConsentDetailsById(row.consentHandle).subscribe(data => {
        var response = data['body'];
        if (response['consentStatus'] == ConsentStatus.ACCEPTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please send data request before',
          })
        } else if (response['consentStatus'] == ConsentStatus.REQUESTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please approve consent',
          })
        } else if (response['consentStatus'] == ConsentStatus.REVOKED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is revoked, You can not check data status',
          })
        } else if (response['consentStatus'] == ConsentStatus.REJECTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is rejected',
          })
        } else {
          this.fiRequestService.getFiStatus(response['requestConsentId'], response['requestSessionId'], row.consentHandle, row.custId).subscribe(data => {
            console.log("Consent FIStatus Response:" + JSON.stringify(data))
            var response = data['body'];
            this.getConsentStaus(row.consentHandle);
            if (response['fiRequestStatus'] == ConsentStatus.PENDING) {
              row.consentStatus = ConsentStatus.PENDING;
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Please send data request again!',
              })
            } else {
              row.consentStatus = ConsentStatus.DATA_READY;
            }
          })
        }
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  onViewData(row) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.getConsentDetailsById(row.consentHandle).subscribe(data => {
        var response = data['body'];
        if (response['consentStatus'] == ConsentStatus.REQUESTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please approve consent',
          })
        } else if (response['consentStatus'] == ConsentStatus.ACCEPTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please send data request before',
          })
        } else if (response['consentStatus'] == ConsentStatus.REVOKED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is revoked, You can not view data',
          })
        } else if (response['consentStatus'] == ConsentStatus.DATA_REQUESTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Please check data status',
          })
        } else if (response['consentStatus'] == ConsentStatus.REJECTED) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is rejected',
          })
        } else if (response['consentStatus'] == ConsentStatus.DATA_READY) {
          this.fiRequestService.getFiFetchData(response['custId'], response['requestConsentId'], response['requestSessionId']).subscribe(data => {
            this.fiObjectsData = data['body'];;
            console.log("fiObjects: ", JSON.stringify(this.fiObjectsData));
          })
          this.consentDetailsPage = false,
            this.transactionDetailsPage = false,
            this.accountsInsightPage = true
        } else {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'You can not view data, Data is not ready.',
          })
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  fetchUsersConsentData() {
    if (sessionStorage.getItem("authorizationToken") != null) {

      var consentRequestDate = this.customerConsentForm.get('consentRequestDate').value;
      var customerId = this.customerConsentForm.get('customerId').value.trim();

      if (consentRequestDate === undefined) {
        consentRequestDate = null;
      }

      if ((consentRequestDate === null || consentRequestDate === '') && customerId !== '') {
        consentRequestDate = null;
        this.custAAId = customerId;
        this.isConsentDateAndCustomerResult = false;
        this.isConsentDateResult = false
        this.isCustomerResult = true;
      } else if ((consentRequestDate === null || consentRequestDate === '') && customerId === '') {
        this.isCustomerResult = false;
        this.isConsentDateResult = false;
        this.isConsentDateAndCustomerResult = false;
      } else if (consentRequestDate !== null && customerId === '') {
        this.consentRequestDate = consentRequestDate;
        this.isCustomerResult = false;
        this.isConsentDateAndCustomerResult = false;
        this.isConsentDateResult = true;
      } else {
        this.custAAId = customerId;
        this.consentRequestDate = consentRequestDate;
        this.isConsentDateAndCustomerResult = true;
        this.isCustomerResult = false;
        this.isConsentDateResult = false;
      }

      console.log("consent request date : " + consentRequestDate + "\t cutomer id : " + customerId);
      if (customerId !== '' && (customerId.indexOf("@") == -1 || customerId.indexOf("@") == 0 || (customerId.length - 1 - customerId.indexOf("@")) == 0)) {
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Invalid Account Aggregator Id.(Eg.<yourid>@<aa handle>.)'
        })
      } else {

        this.consentDetailsList = null;
        this.consentRequestService.getConsentDetails(customerId, consentRequestDate).subscribe(data => {
          if (data['errors']) {
            console.log("Consent Response: ", JSON.stringify(data['errors']));
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else {
            var bodyResponse = data["body"];

            if (typeof bodyResponse == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found.'
              })
            }

            this.consentDetailsList = bodyResponse[this.TAKE_VALUE_FROM_JSON_NAME.CONSENT_DETAILS_LIST];
            this.tempConsentDetailsList = this.consentDetailsList
          }

        });
      }

    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  getConsentStaus(consentHandle) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.getConsentDetailsById(consentHandle).subscribe(data => {
        var response = data['body'];
        this.consentStatus = response['consentStatus'];
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
    return this.consentStatus;
  }

  onAccountInsightData(custId, requestConsentId, requestSessionId, fiAccountInfo, accountRefNo) {
    var products = fiAccountInfo.filter((x) => x.accountRefNo.includes(accountRefNo))
    console.log(JSON.stringify(products));
    console.log(products[0].linkRefNo);

    this.subscription = this.accountInsightService.getAccountInsight(custId, requestConsentId, requestSessionId, products[0].linkRefNo).subscribe(data => {
      if (data != null) {
        this.checkData = false;
        this.consentDetailsPage = false;
        this.transactionDetailsPage = true;
        this.accountsInsightPage = false;
        console.log(JSON.stringify(data));
        this.fiDetail = data['fiDetail']
        this.period = data['period'];
        this.transactions = data['transactions'];
        this.monthlySurplus = data['monthlySurplus'];
        this.monthlyAvgBalance = data['monthlyAvgBalance']
        this.topCredits = data['topCredits'];
        this.recurringCredits = data['recurringCredits'];
        this.topDebits = data['topDebits'];
        this.recurringDebits = data['recurringDebits'];
        this.flowAnalysis = data['flowAnalysis'];
        this.overdrafts = data['overdrafts'];
        this.autoDebits = data['autoDebits'];
        this.topCashTxns = data['topCashTxns'];
        this.topChequeTxns = data['topChequeTxns'];
        this.subscription.unsubscribe();

        var credit = this.flowAnalysis['totalCredits']
        var totalCredit = credit['totalAmount']
        console.log("totalCredit: ", totalCredit);

        var debit = this.flowAnalysis['totalDebits']
        var totalDebit = debit['totalAmount']
        console.log("totalDebit: ", totalDebit);

        var totalAmount = totalCredit + totalDebit;
        console.log("totalAmount: ", totalAmount);

        this.creditPercent = totalCredit / totalAmount * 100;
        console.log("Total CrgetStatus(consentHandleId)edit:", this.creditPercent)

        this.debitPercent = totalDebit / totalAmount * 100;
        console.log("Total Debit:", this.debitPercent);

        this.transactions.sort(function (a, b) {
          return a.srNo - b.srNo;
        });
        console.log(this.transactions);
      } else {
        this.accountsInsightPage = false;
        alert("Accont Data not found.")
      }
      this.flowAnaylsisPieChartData = {
        chartType: 'PieChart',
        dataTable: [
          ['Task', 'Percentage'],
          ['Total Credit', this.creditPercent],
          ['Total Debits', this.debitPercent]
        ],
        options: {
          height: 300,
          pieHole: 0.3,
          colors: ['#008000', '#FF0000'],
          legend: { position: 'right', maxLines: 2 }
        },
      };
    }, (err: any) => alert('Account Data not found.'));
  }

  onclickRow1(i, type) {
    this.collapsed1 = !this.collapsed1;
    if (type == 'DEPOSIT') {
      this.depositType = true;
      this.termDeposiType = false;
      this.depositIfsc = true;
      this.termDepositIfsc = false;
      this.depositTrasactionTimestamp = true;
      this.termDepositTransactionDateTime = false;
      this.depositCurrentBalance = true;
      this.termDepositCurrentValue = false;

    } else if (type == 'TERM-DEPOSIT') {
      this.termDeposiType = true;
      this.depositType = false;
      this.depositIfsc = false;
      this.termDepositIfsc = true;
      this.depositTrasactionTimestamp = false;
      this.termDepositTransactionDateTime = true;
      this.depositCurrentBalance = false;
      this.termDepositCurrentValue = true;
    } else {
      console.log("Something went wrong!");
    }
  }

  onclickRow() {
    this.collapsed = !this.collapsed;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.tempConsentDetailsList.filter(function (d) {
      return d.custId.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.consentDetailsList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  callFiRequest(row) {

    var fiRequestSummaryData = JSON.parse(this.fiRequestSummaryData)
    this.fiRequestService.createFiRequest(fiRequestSummaryData).subscribe(data => {
      var resultData = data['body'];
      for (let key in resultData) {
        this.resultKey = resultData[key];
      }
      console.log("this.resultKey" + JSON.stringify(this.resultKey))
      if (this.resultKey == null) {
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Your data request has been sent!!'
        })
        console.log("FiRequest Response: " + JSON.stringify(data['body']));
        row.consentStatus = ConsentStatus.DATA_REQUESTED;
      } else {
        if (this.resultKey.errorMsg.includes('InvalidConsentStatus')) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent is revoked'
          })
          console.log("FiRequest Response: " + JSON.stringify(data['body']));
          this.fiRequestService.updateCustomerStatus(fiRequestSummaryData.custId, fiRequestSummaryData.consentHandleId, "REVOKED");
          row.consentStatus = ConsentStatus.REVOKED;
        } else if (this.resultKey.errorMsg.includes('Consent already used for allowed units for the frequncy.')) {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Consent already used for allowed units for the frequncy'
          })
          console.log("FiRequest Response: " + JSON.stringify(data['body']));
          this.fiRequestService.updateCustomerStatus(fiRequestSummaryData.custId, fiRequestSummaryData.consentHandleId, "EXPIRED");
          row.consentStatus = ConsentStatus.EXPIRED;
        } else {
          alert("Something went wrong!")
        }
      }
    });
  }

  showPromptOnNo() {
    this.modal.style.display = "none";
    this.simpleModalService.addModal(AlertComponent, {
      message: 'Your data request has been cancelled!!'
    })
  }

  onBackClick() {
    this.transactionDetailsPage = !this.transactionDetailsPage;
    this.consentDetailsPage = !this.consentDetailsPage;
  }

  onBack() {
    this.consentDetailsPage = !this.consentDetailsPage;
    this.accountsInsightPage = !this.accountsInsightPage;
  }
}