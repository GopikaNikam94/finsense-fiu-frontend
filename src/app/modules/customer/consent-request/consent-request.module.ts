import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsentRequestRoutingModule } from './consent-request-routing.module';
import { ConsentRequestComponent } from './consent-request.component';
import { SelectOptionService } from '../../../shared/elements/select-option.service';
import { SelectModule } from 'ng-select';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ConsentRequestRoutingModule,
    SelectModule
  ],
  declarations: [ConsentRequestComponent],
  providers: [SelectOptionService]
})
export class ConsentRequestModule { }
