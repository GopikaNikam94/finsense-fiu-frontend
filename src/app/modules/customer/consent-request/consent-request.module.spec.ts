import { ConsentRequestModule } from './consent-request.module';

describe('ConsentRequestModule', () => {
  let consentRequestModule: ConsentRequestModule;

  beforeEach(() => {
    consentRequestModule = new ConsentRequestModule();
  });

  it('should create an instance', () => {
    expect(consentRequestModule).toBeTruthy();
  });
});
