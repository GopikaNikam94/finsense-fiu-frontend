import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Customer',
      status: false
    },
    children: [
      {
        path: 'consent-request',
        loadChildren: './consent-request/consent-request.module#ConsentRequestModule'
      },
      {
        path: 'customer-list',
        loadChildren: './customer-list/customer-list.module#CustomerListModule'
      },
      {
        path: 'customer-consent',
        loadChildren: './customer-consent/customer-consent.module#CustomerConsentModule'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }



