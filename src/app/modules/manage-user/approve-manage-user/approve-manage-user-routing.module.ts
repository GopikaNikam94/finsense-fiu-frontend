import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveManageUserComponent } from './approve-manage-user.component';

const routes: Routes = [
  {
    path: '',
    component: ApproveManageUserComponent,
    data: {
      title: 'Approve Manage User',
      icon: 'ti-anchor',
      caption: 'Approve Manage User',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveManageUserRoutingModule { }
