import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateManageUserComponent } from './create-manage-user.component';

describe('CreateManageUserComponent', () => {
  let component: CreateManageUserComponent;
  let fixture: ComponentFixture<CreateManageUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateManageUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateManageUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
