import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users',
      status: false
    },
    children: [
      {
        path: 'approve-manage-user',
        loadChildren: './approve-manage-user/approve-manage-user.module#ApproveManageUserModule'
      },
      {
        path: 'create-manage-user',
        loadChildren: './create-manage-user/create-manage-user.module#CreateManageUserModule'
      },
      {
        path: 'pending-manage-user',
        loadChildren: './pending-manage-user/pending-manage-user.module#PendingManageUserModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageUserRoutingModule { }
