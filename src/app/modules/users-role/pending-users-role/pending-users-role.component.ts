import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { UserRoleService } from 'src/app/services/user-role/user-role.service';
import { ApproveUserRole, UserRole } from 'src/app/services/user-role/user-role';
import { AlertComponent } from '../../alert/alert.component';
import { SimpleModalService } from 'ngx-simple-modal';

@Component({
  selector: 'ngbd-modal-approve',
  styleUrls: ['./pending-users-role.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to approve consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="approveUserRole(data)">
                    Approve
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalApprove {
 @Input() data

  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService, 
              public userRoleService: UserRoleService) {}

    approveUserRole(data) {
      if(sessionStorage.getItem("authorizationToken") != null) {
        const approveUserRole : ApproveUserRole = {
          roleName: data['roleName']
        }
      
        this.userRoleService.approveUserRole(approveUserRole).subscribe(data => {
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else if(data['body']) {
            this.simpleModalService.addModal(AlertComponent, {
              message: "User role has been approved"
            })
          } else {
            if (typeof data["body"] == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found'
              })
            }
          }      
          console.log("Approved User Role Response: ", JSON.stringify(data));
          this.modal.close();
          this.router.navigate(['/users-role/approve-users-role/']);
        });
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
    }  
  }

@Component({
  selector: 'ngbd-modal-reject',
  styleUrls: ['./pending-users-role.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to reject consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteUserRole(data)">
                    Reject
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalReject {
 @Input() data
  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService,
              public userRoleService: UserRoleService) {}

    deleteUserRole(data) {
      if(sessionStorage.getItem("authorizationToken") != null) {
          this.userRoleService.deleteUserRole(data['roleName']).subscribe(data => {
            console.log("Deleted User Role Response: "+ data);
          });
          this.simpleModalService.addModal(AlertComponent, {
            message: 'User role has been rejected.'
          })
          this.modal.close();
          this.router.navigate(['/users-role/approve-users-role/']);
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
   }
}

@Component({
  selector: 'app-pending-users-role',
  templateUrl: './pending-users-role.component.html',
  styleUrls: ['./pending-users-role.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class PendingUsersRoleComponent implements OnInit {

  public userList = true; viewUpdatedUserRole = true; viewUserRole = true;
  public userRolesList = []; permissionGroups = [];
  public userRole: any; updatedUserRole: any; userRolesResult: any; result: any;
  public roleName: string; roleDescription: string; createdBy: string; updatedBy: string;
          action: string;
  public creationDate: Date; updationDate: Date;


  constructor(public modalService: NgbModal, public router: Router,
              public simpleModalService: SimpleModalService, 
              public userRoleService: UserRoleService) {
    this.fetchUserRoleList();
  }

  ngOnInit() {}

  getApproveModal(data: any) {
    if(data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user role"        
      })
    } else if(data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user role"        
      })
    } else {
        this.openApproveModal(data);
    }
  }

  openApproveModal(data: any) {
    const approveModalRef =  this.modalService.open(NgbdModalApprove,{ centered: true });
    approveModalRef.componentInstance.data = data;
  }

  getRejectModal(data: any) {
    if(data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user role"        
      })
    } else if(data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user role"        
      })
    } else {
      this.openRejectModal(data);
    }
  }

  openRejectModal(data: any) {
    const rejectModalRef =  this.modalService.open(NgbdModalReject,{ centered: true });
    rejectModalRef.componentInstance.data = data;
  }

  fetchUserRoleList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchUserRoleList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.userRolesResult = data['body']
          this.userRolesList = this.userRolesResult.userRoles
          console.log("User Role List: "+ JSON.stringify(this.userRolesResult));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewUserRoleRecord(roleName) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchUserRole(roleName).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          console.log("View User Role Response: "+ JSON.stringify(data));
          this.result = data['body']
          this.viewUserDetails(this.setValueToObject(this.result.userRole))
          this.userList = false;
          this.viewUpdatedUserRole = false;
          this.fetchApprovedUserRole(roleName);
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    } 
  }

  fetchApprovedUserRole(rolName) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchApprovedUserRole(rolName).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          console.log("Approved User Role Details: "+ JSON.stringify(data));
          this.result = data['body']
          this.viewUserDetail(this.setValueToObject(this.result.userRole));
          if(this.userRole != null) {
            if(this.userRole.roleName == this.updatedUserRole.roleName) {
              this.userList = false;
              this.viewUserRole = false;
              this.viewUpdatedUserRole = true;
              this.setValues(this.updatedUserRole, this.userRole);
            }
          }
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
  
  setValues(updatedUserRole, userRole) {
    if(updatedUserRole.userId != userRole.roleName) {
       this.roleName = updatedUserRole.userId
    }

    if(updatedUserRole.roleDescription != userRole.roleDescription) {
      this.roleDescription = updatedUserRole.roleDescription
    }

    if(JSON.stringify(updatedUserRole.permissionGroups) != JSON.stringify(userRole.permissionGroups)) {
      this.permissionGroups = updatedUserRole.permissionGroups
    }
 
    if(updatedUserRole.action != userRole.action) {
      this.action = updatedUserRole.action
    }  

    if(updatedUserRole.createdBy != userRole.createdBy) {
      this.createdBy = updatedUserRole.createdBy
    }  
    
    if(updatedUserRole.creationDate != userRole.creationDate) {
      this.creationDate = updatedUserRole.creationDate
    } 
    
    if(updatedUserRole.updatedBy != userRole.updatedBy) {
      this.updatedBy = updatedUserRole.updatedBy
    }  
    
    if(updatedUserRole.updationDate != userRole.updationDate) {
      this.updationDate = updatedUserRole.updationDate
    } 
  }

  viewUserDetails(updatedUserRole){
    this.updatedUserRole = updatedUserRole;
  }

  viewUserDetail(userRole){
    this.userRole = userRole;
  }

  setValueToObject(data) {
    const userRole: UserRole = {
      roleName:              data['roleName'],
      roleDescription:       data['roleDescription'],
      permissionGroupList:   data['permissionGroupList'],
      permissionGroups:      data['permissionGroups'],
      permissions:           data['permissions'],
      action:                data['action'],
      active:                data['active'],
      createdBy:             data['createdBy'],
      creationDate:          data['creationDate'],
      updatedBy:             data['updatedBy'],
      updationDate:          data['updationDate'],
      approvedBy:            data['approvedBy'],
      approvedDate:          data['approvedDate']
    }
    return userRole;
}

  toggleCloseUserRecord() {
    this.userList = true;
    this.viewUpdatedUserRole = true;
    this.viewUserRole = true;
  }
}
