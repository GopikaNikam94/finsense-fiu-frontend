import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingUsersRoleComponent } from './pending-users-role.component';

const routes: Routes = [
  {
    path: '',
    component: PendingUsersRoleComponent,
    data: {
      title: 'Pending Users Role',
      icon: 'ti-anchor',
      caption: 'Pending Users Role',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingUsersRoleRoutingModule { }
