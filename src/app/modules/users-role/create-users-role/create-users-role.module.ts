import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUsersRoleRoutingModule } from './create-users-role-routing.module';
import { CreateUsersRoleComponent } from './create-users-role.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    CreateUsersRoleRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  declarations: [ CreateUsersRoleComponent ]
})
export class CreateUsersRoleModule { }
