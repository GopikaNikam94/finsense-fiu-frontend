import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveUsersRoleComponent } from './approve-users-role.component';

describe('ApproveUsersRoleComponent', () => {
  let component: ApproveUsersRoleComponent;
  let fixture: ComponentFixture<ApproveUsersRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveUsersRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveUsersRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
