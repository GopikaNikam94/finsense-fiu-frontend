import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { Channel } from 'src/app/services/admin-function-services/channel/channel';
import { ChannelService } from 'src/app/services/admin-function-services/channel/channel.service';
import { AlertComponent } from '../../alert/alert.component';


@Component({
  selector: 'ngbd-modal-delete',
  styleUrls: ['./approve-channel.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template: 
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to delete consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-cancel mb-2 mr-2" 
                    (click)="modal.dismiss('cancel click')">
                    Cancel
            </button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteChannel(channelId)">
                    Delete
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalDelete {
 @Input() channelId;

  constructor(public modal: NgbActiveModal, 
              private router: Router, 
              private simpleModalService: SimpleModalService, 
              private channelService: ChannelService ) {}
            
    deleteChannel(channelId) {
      if(sessionStorage.getItem("authorizationToken") != null) {
        this.channelService.deleteApprovedChannel(channelId).subscribe(data => { 
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Channel has been deleted"
            })
          }      
          this.modal.close();
          console.log("Deleted approved channel response: "+ data['body']);
          this.router.navigate(['/channels/pending-channel/']);
        });
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
    }
}

@Component({
  selector: 'app-approve-channel',
  templateUrl: './approve-channel.component.html',
  styleUrls: ['./approve-channel.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class ApproveChannelComponent implements OnInit {

  public updateChannelForm: FormGroup;
  public channelList: any; channelRowData: any; profileNamesList: any; creationDate: any;
  public channelsList = true; viewChannel = true; editChannel = true;
  public selectedChannelEnabled: string; profileId: string; createdBy: string;    

  constructor(private channelService: ChannelService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private simpleModalService: SimpleModalService) {
    this.updateChannelForm = this.formBuilder.group({
      updateChannelId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.channelNameIdLength])],
      updateChannelName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.channelNameIdLength])],
      updateProfileName: ['', Validators.required],
      updateChannelEnabled: ['', Validators.required]
    })
    this.fetchApprovedChannelList();
  }

  ngOnInit() {
  }

  openDeleteModal(channelId: string) {
    const deleteModalRef =  this.modalService.open(NgbdModalDelete,{ centered: true });
    deleteModalRef.componentInstance.channelId = channelId;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  public channelNameIdLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 32;
    const isValid = !islength;
    return isValid ? null : { 'channelNameIdLength': true };
  }

  fetchApprovedChannelList() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.channelService.fetchApprovedChannelList().subscribe(data => {
        var response = data['body']
        console.log("Approved Channel List Response: " + response);
        this.channelList = response['channel'];
        this.profileNamesList = response['profileNameList'];
        this.channelList = this.channelList.filter(a => a.active == 'Y');
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewChannelRecord(channelId) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.channelService.fetchApprovedChannel(channelId).subscribe(data => {
        var response = data['body'];
        console.log("Channel Response: " + response['channel']);
        this.viewChannelDetails(response['channel']);
        this.channelsList = false;
        this.viewChannel = false;
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  editChannelRecord(channelId) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.channelService.fetchApprovedChannel(channelId).subscribe(data => {
        console.log("Approved Consent Response: " + JSON.stringify(data));
        var response = data['body'];
        this.getUpdateRow(response['channel']);
        this.channelsList = false;
        this.viewChannel = true;
        this.editChannel = false;
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  getUpdateRow(channelData) {
    this.profileId = channelData.profileId;
    this.selectedChannelEnabled = channelData.enabled;
    this.createdBy = channelData.createdBy,
    this.creationDate = channelData.creationDate   

    this.updateChannelForm.setValue({
      updateChannelId: channelData.channelId,
      updateChannelName: channelData.channelName,
      updateProfileName: channelData.profileName,
      updateChannelEnabled: channelData.enabled
    })
  }

  updateChannel() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      const channel: Channel = {
        channelId: this.updateChannelForm.get('updateChannelId').value,
        channelName: this.updateChannelForm.get('updateChannelName').value,
        profileId: this.profileId,
        enabled: this.selectedChannelEnabled,
        keepAliveTimeout: null,
        keepAliveUrl: null,
        profileName: this.updateChannelForm.get('updateProfileName').value,
        action: null,
        active: null,
        createdBy: this.createdBy,
        creationDate: this.creationDate,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

      if (sessionStorage.getItem("authorizationToken") != null) {
        this.channelService.updateChannel(channel).subscribe(data => {
          if (data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else if (data['body']) {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Channel has been updated"
            })
          } else {
            if (typeof data["body"] == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found'
              })
            }
          }
          console.log(" Update Channel Response: ", JSON.stringify(data));
          this.fetchApprovedChannelList();
          this.router.navigate(['/channels/pending-channel'])
        })
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  onChannelChange(event) {
    if (event.target.checked) {
      this.selectedChannelEnabled = "Y";
    } else {
      this.selectedChannelEnabled = "N";
    }
  }

  viewChannelDetails(channelData) {
    this.channelRowData = channelData;
  }

  toggleCloseChannelRecord() {
    this.channelsList = true;
    this.viewChannel = true;
  }

  toggleEditChannel() {
    this.editChannel = true;
    this.channelsList = true;
    this.viewChannel = true;
  }

  submitForm() {
    this.markFormTouched(this.updateChannelForm);
    if (this.updateChannelForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.updateChannelForm.getRawValue;
      this.updateChannel();
    } else {
      alert("Validation Failed");
    }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) {
        control.markAsTouched();
        this.markFormTouched(control);
      } else {
        control.markAsTouched();
      };
    });
  };
}
