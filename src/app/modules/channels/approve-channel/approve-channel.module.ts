import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectModule } from 'ng-select';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { ApproveChannelRoutingModule } from './approve-channel-routing.module';
import { ApproveChannelComponent, NgbdModalDelete } from './approve-channel.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ApproveChannelRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ ApproveChannelComponent, NgbdModalDelete ],
  providers: [ SelectOptionService ],
  bootstrap: [ ApproveChannelComponent ],
  entryComponents: [ NgbdModalDelete ]
})
export class ApproveChannelModule { }
