import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { ApproveChannel, Channel } from 'src/app/services/admin-function-services/channel/channel';
import { ChannelService } from 'src/app/services/admin-function-services/channel/channel.service';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-approve',
  styleUrls: ['./pending-channel.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to approve channel</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="approveChannel(data)">
                    Approve
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalApprove {
  @Input() data

  constructor(public modal: NgbActiveModal, 
              private router: Router,
              private simpleModalService: SimpleModalService,
              private channelService: ChannelService) { }

    approveChannel(data) {
      const approveChannel: ApproveChannel = {
        channelId: data['channelId']
    }

    this.channelService.createApprovedChannel(approveChannel).subscribe(data => {
      if (data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if (data['body']) {
        this.simpleModalService.addModal(AlertComponent, {
          message: "Channel has been approved"
        })
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }
      console.log("Approved Channel Response: ", JSON.stringify(data));
      this.modal.close();
      this.router.navigate(['/channels/approve-channel/']);
    });
  }
}

@Component({
  selector: 'ngbd-modal-reject',
  styleUrls: ['./pending-channel.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to reject channel</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="rejectChannel(data)">
                    Reject
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalReject {
  @Input() data
  constructor(public modal: NgbActiveModal, 
              private router: Router,
              private simpleModalService: SimpleModalService, 
              private channelService: ChannelService) { }

  rejectChannel(data) {
    this.channelService.deleteChannel(data['channelId']).subscribe(data => {
      console.log("Deleted Channel Response: " + data);
    });
    this.simpleModalService.addModal(AlertComponent, {
      message: 'Your channel has been rejected.'
    })
    this.modal.close();
    this.router.navigate(['/channels/approve-channel/']);
  }
}

@Component({
  selector: 'app-pending-channel',
  templateUrl: './pending-channel.component.html',
  styleUrls: ['./pending-channel.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class PendingChannelComponent implements OnInit {

  public channelList: any; channelUpdatedRowData: any; channelRowData: any;
  public channelsList = true; viewUpdatedChannel = true; viewChannel = true;
  public  channelId: string; channelName: string; profileId: string; enabled: string;
  keepAliveTimeout: number; keepAliveUrl: string;	profileName: string; action: string;
  active: string; createdBy: string; creationDate: string; updatedBy: string; updationDate: string;

  constructor(private channelService: ChannelService,
              private router: Router,
              private simpleModalService: SimpleModalService,
              private modalService: NgbModal) {
    this.fetchChannelList();
  }

  getApproveModal(data: any) {
    if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving channel"
      })
    } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving channel"
      })
    } else {
      this.openApproveModal(data);
    }
  }

  openApproveModal(data: any) {
    const approveModalRef = this.modalService.open(NgbdModalApprove, { centered: true });
    approveModalRef.componentInstance.data = data;
  }

  getRejectModal(data: any) {
    if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving channel"
      })
    } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving channel"
      })
    } else {
      this.openRejectModal(data);
    }
  }

  openRejectModal(data: any) {
    const rejectModalRef = this.modalService.open(NgbdModalReject, { centered: true });
    rejectModalRef.componentInstance.data = data;
  }

  ngOnInit() { }

  fetchChannelList() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.channelService.fetchChannelList().subscribe(data => {
        var response = data['body']
        console.log("Channel List Response: " + data);
        this.channelList = response['channel'];
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewChannelRecord(channelId) {
    this.channelService.fetchChannel(channelId).subscribe(data => {
      console.log("Channel Response: " + JSON.stringify(data));
      var response = data['body'];
      this.viewChannelDetails(this.setValueToObject(response['channel']));
      this.channelsList = false;
      this.viewUpdatedChannel = false;
      this.fetchApprovedChannel(channelId);
    });
  }

  fetchApprovedChannel(channelId) {
    this.channelService.fetchApprovedChannel(channelId).subscribe(data => {
      console.log("Approved Channel Response: " + JSON.stringify(data));
      var response = data['body'];
      if (typeof JSON.stringify(data) == 'undefined' || response['channel'] == null) {
        console.log("Data not found")
      } else if (data['errors']) {
        console.log("Error: ", JSON.stringify(data['errors']));
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
        }
      } else {
        console.log("Approved Channel Response: " + JSON.stringify(data['body']));
        var response = data['body'];
        console.log("response: " + JSON.stringify(response));
        this.viewChannelDetail(this.setValueToObject(response['channel']));
        if (this.channelRowData != null) {
          if (this.channelRowData.channelId == this.channelUpdatedRowData.channelId) {
            this.channelsList = false;
            this.viewUpdatedChannel = true;
            this.viewChannel = false;
            this.setValues(this.channelRowData, this.channelUpdatedRowData);
          }
        }
      }
    });
  }

  viewChannelDetails(channelUpdatedRowData) {
    this.channelUpdatedRowData = channelUpdatedRowData;
  }

  viewChannelDetail(channelRowData) {
    this.channelRowData = channelRowData;
  }

  setValueToObject(data) {
    const channel: Channel = {
      channelId:  data['channelId'],
      channelName:  data['channelName'],
      profileId:  data['profileId'],
      enabled: data['enabled'],
      keepAliveTimeout: data['keepAliveTimeout'],
      keepAliveUrl: data['keepAliveUrl'],	
      profileName: data['profileName'],
      action: data['action'],
      active: data['active'],
      createdBy: data['createdBy'],
      creationDate: data['creationDate'],
      updatedBy: data['updatedBy'],
      updationDate: data['updationDate'],
      approvedBy: data['approvedBy'],
      approvedDate: data['approvedDate']
    }
    return channel;
  }

  setValues(channelRowData, channelUpdatedRowData) {
    if (channelUpdatedRowData.channelName != channelRowData.channelName) {
      this.channelName = channelUpdatedRowData.channelName;
    }

    if (channelUpdatedRowData.profileId != channelRowData.profileId) {
      this.profileId = channelUpdatedRowData.profileId;
    }

    if (channelUpdatedRowData.enabled != channelRowData.enabled) {
      this.enabled = channelUpdatedRowData.enabled;
    }
    
    if (channelUpdatedRowData.keepAliveTimeout != channelRowData.keepAliveTimeout) {
      this.keepAliveTimeout = channelUpdatedRowData.keepAliveTimeout;
    }
    
    if (channelUpdatedRowData.keepAliveUrl != channelRowData.keepAliveUrl) {
      this.keepAliveUrl = channelUpdatedRowData.keepAliveUrl;
    }
    
    if (channelUpdatedRowData.profileName != channelRowData.profileName) {
      this.profileName = channelUpdatedRowData.profileName;
    }
    
    if (channelUpdatedRowData.action != channelRowData.action) {
      this.action = channelUpdatedRowData.action;
    }
    
    if (channelUpdatedRowData.active != channelRowData.active) {
      this.active = channelUpdatedRowData.active;
    }

    if (channelUpdatedRowData.createdBy != channelRowData.createdBy) {
      this.createdBy = channelUpdatedRowData.createdBy;
    }

    if (channelUpdatedRowData.creationDate != channelRowData.creationDate) {
      this.creationDate = channelUpdatedRowData.creationDate;
    }

    if (channelUpdatedRowData.updatedBy != channelRowData.updatedBy) {
      this.updatedBy = channelUpdatedRowData.updatedBy;
    }

    if (channelUpdatedRowData.updationDate != channelRowData.updationDate) {
      this.updationDate = channelUpdatedRowData.updationDate;
    }
  }

  toggleCloseChannelRecord() {
    this.channelsList = true;
    this.viewUpdatedChannel = true;
    this.viewChannel = true;
  }
}
