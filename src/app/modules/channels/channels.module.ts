import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelsRoutingModule } from './channels-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ChannelsRoutingModule
  ],
  declarations: []
})
export class ChannelsModule { }
