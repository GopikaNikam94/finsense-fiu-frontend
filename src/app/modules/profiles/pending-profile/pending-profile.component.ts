import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { ProfileService } from 'src/app/services/admin-function-services/profile/profile.service';
import { ApproveProfile, Profile } from 'src/app/services/admin-function-services/profile/profile';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-approve',
  styleUrls: ['./pending-profile.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to approve profile</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="approveProfile(data)">
                    Approve
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalApprove {
  @Input() data

  constructor(public modal: NgbActiveModal, 
              public router: Router,
              public simpleModalService: SimpleModalService, 
              public profileService: ProfileService) { }

  approveProfile(data) {
    const approveProfile: ApproveProfile = {
      profileId: data['profileId']
    }

    this.profileService.createApprovedProfile(approveProfile).subscribe(data => {
      console.log("Profile Response: ", JSON.stringify(data));
      if (data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if (data['body']) {
        this.simpleModalService.addModal(AlertComponent, {
          message: "Profile has been approved"
        })
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }
      this.modal.close();
      this.router.navigate(['/profiles/approve-profile/']);
    });
  }
}

@Component({
    selector: 'ngbd-modal-reject',
    styleUrls: ['./pending-profile.component.scss',
      '../../../../assets/icon/icofont/css/finsense-common.scss'],
    template:
      `<div class="modal-body">
        <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to reject channel</p>
        <div class="row" style="margin-top: 18px;align: center" align="center">
          <div class="col-6">
              <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
          </div>
          <div class="col-6">
              <button type="button" 
                      class="btn btn-outline-primary mb-2 mr-2" 
                      (click)="rejectProfile(data)">
                      Reject
              </button>
          </div>
        </div>
    </div>`
  })
  
  export class NgbdModalReject {
    @Input() data
    constructor(public modal: NgbActiveModal, 
                private router: Router,
                private simpleModalService: SimpleModalService, 
                private profileService: ProfileService) { }
  
    rejectProfile(data) {
      this.profileService.deleteProfile(data['profileId']).subscribe(data => {
        console.log("Deleted Profile Response: " + data);
      });
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Your profile has been rejected.'
      })
      this.modal.close();
      this.router.navigate(['/profiles/approve-profile/']);
    }
  }
  
@Component({
  selector: 'app-pending-profile',
  templateUrl: './pending-profile.component.html',
  styleUrls: ['./pending-profile.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})

export class PendingProfileComponent implements OnInit {

    @ViewChild(DatatableComponent) table: DatatableComponent;
  
    public profileList: any = []; profileUpdatedRowData: any; profileRowData: any; profileApis: any = [];
    public profilesList = true; viewUpdatedProfile = true; viewProfile = true;
    public profileId: string; profileName: string; profileDescription: string; action: string;
            active: string; createdBy: string; updatedBy: string; 
    public creationDate: Date; updationDate: Date;

    constructor(private profileService: ProfileService,
                private router: Router,
                private simpleModalService: SimpleModalService,
                private modalService: NgbModal) {
      this.fetchProfileList();
    }

    ngOnInit() {}

    getApproveModal(data: any) {
        if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
        this.simpleModalService.addModal(AlertComponent, {
            message: "Creator " + data['createdBy'] + " and approver "
            + sessionStorage.getItem('currentUser') + " can not be same while approving profile"
        })
        } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
        this.simpleModalService.addModal(AlertComponent, {
            message: "Updater " + data['updatedBy'] + " and approver "
            + sessionStorage.getItem('currentUser') + " can not be same while approving profile"
        })
        } else {
        this.openApproveModal(data);
        }
    }

    openApproveModal(data: any) {
        const approveModalRef = this.modalService.open(NgbdModalApprove, { centered: true });
        approveModalRef.componentInstance.data = data;
    }

    getRejectModal(data: any) {
        if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
        this.simpleModalService.addModal(AlertComponent, {
            message: "Creator " + data['createdBy'] + " and approver "
            + sessionStorage.getItem('currentUser') + " can not be same while approving profile"
        })
        } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
        this.simpleModalService.addModal(AlertComponent, {
            message: "Updater " + data['updatedBy'] + " and approver "
            + sessionStorage.getItem('currentUser') + " can not be same while approving profile"
        })
        } else {
        this.openRejectModal(data);
        }
    }

    openRejectModal(data: any) {
        const rejectModalRef = this.modalService.open(NgbdModalReject, { centered: true });
        rejectModalRef.componentInstance.data = data;
    }

    fetchProfileList() {
        if (sessionStorage.getItem("authorizationToken") != null) {
        this.profileService.fetchProfileList().subscribe(data => {
            var response = data['body']
            console.log("Profile List Response: " + JSON.stringify(data));
            this.profileList = response['profiles'];
        })
        } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
        }
    }

    viewProfileRecord(profileId) {
        this.profileService.fetchProfile(profileId).subscribe(data => {
        console.log("Profile Response: " + JSON.stringify(data));
        var response = data['body'];
        this.viewProfileDetails(this.setValueToObject(response['profile']));
        this.profilesList = false;
        this.viewUpdatedProfile = false;
        this.fetchApprovedProfile(profileId);
        });
    }
 
    fetchApprovedProfile(profileId) {
        this.profileService.fetchApprovedProfile(profileId).subscribe(data => {
        console.log("Approved Profile Response: " + JSON.stringify(data));
        var response = data['body'];
        if (typeof JSON.stringify(data) == 'undefined' || response['profile'] == null) {
            console.log("Data not found")
        } else if (data['errors']) {
            console.log("Error: ", JSON.stringify(data['errors']));
            var data1 = data['errors'];
            for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            }
        } else {
            var response = data['body'];
            console.log("response: " + JSON.stringify(response));
            this.viewProfileDetail(this.setValueToObject(response['profile']));
            if (this.profileRowData != null) {
            if (this.profileRowData.profileId == this.profileUpdatedRowData.profileId) {
                this.profilesList = false;
                this.viewUpdatedProfile = true;
                this.viewProfile = false;
                this.setValues(this.profileRowData, this.profileUpdatedRowData);
            }
          }
        }
        });
    }

    viewProfileDetails(profileUpdatedRowData) {
        this.profileUpdatedRowData = profileUpdatedRowData;
    }

    viewProfileDetail(profileRowData) {
        this.profileRowData = profileRowData;
    }

    setValueToObject(data) {
        const profile: Profile = {
            profileId:  data['profileId'],
            profileName: data['profileName'],
            profileDescription: data['profileDescription'],
            profileApis: data['profileApis'],
            action: data['action'],
            active: data['active'],
            createdBy: data['createdBy'],
            creationDate: data['creationDate'],
            updatedBy: data['updatedBy'],
            updationDate: data['updationDate'],
            approvedBy: data['approvedBy'],
            approvedDate: data['approvedDate']
        }
        return profile;
    }

    setValues(profileRowData, profileUpdatedRowData) {
        if (profileUpdatedRowData.profileId != profileRowData.profileId) {
        this.profileId = profileUpdatedRowData.profileId;
        }
        
        if (profileUpdatedRowData.profileName != profileRowData.profileName) {
        this.profileName = profileUpdatedRowData.profileName;
        }

        if (profileUpdatedRowData.profileDescription != profileRowData.profileDescription) {
            this.profileDescription = profileUpdatedRowData.profileDescription;
        }
        
        if (JSON.stringify(profileUpdatedRowData.profileApis) != JSON.stringify(profileRowData.profileApis)) {
            this.profileApis = profileUpdatedRowData.profileApis;
        }

        if (profileUpdatedRowData.action != profileRowData.action) {
        this.action = profileUpdatedRowData.action;
        }
        
        if (profileUpdatedRowData.active != profileRowData.active) {
        this.active = profileUpdatedRowData.active;
        }

        if (profileUpdatedRowData.createdBy != profileRowData.createdBy) {
        this.createdBy = profileUpdatedRowData.createdBy;
        }

        if (profileUpdatedRowData.creationDate != profileRowData.creationDate) {
        this.creationDate = profileUpdatedRowData.creationDate;
        }

        if (profileUpdatedRowData.updatedBy != profileRowData.updatedBy) {
        this.updatedBy = profileUpdatedRowData.updatedBy;
        }

        if (profileUpdatedRowData.updationDate != profileRowData.updationDate) {
        this.updationDate = profileUpdatedRowData.updationDate;
        }
    }

    toggleCloseProfileRecord() {
        this.profilesList = true;
        this.viewUpdatedProfile = true;
        this.viewProfile = true;
    }
}