import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingProfileComponent } from './pending-profile.component';

const routes: Routes = [
  {
    path: '',
    component: PendingProfileComponent,
    data: {
      title: 'Pending Profile',
      icon: 'ti-anchor',
      caption: 'Pending Profile.',
      status: false
    }
  }
];
0
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingProfileRoutingModule { }
