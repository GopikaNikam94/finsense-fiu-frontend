import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectModule } from 'ng-select';
import { ApproveProfileRoutingModule } from './approve-profile-routing.module';
import { ApproveProfileComponent, NgbdModalDelete } from './approve-profile.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ApproveProfileRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ ApproveProfileComponent, NgbdModalDelete ],
  providers: [ SelectOptionService ],
  bootstrap: [ ApproveProfileComponent ],
  entryComponents: [ NgbdModalDelete ]
})
export class ApproveProfileModule { }
